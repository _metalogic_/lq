FROM golang:1.15 AS builder

WORKDIR /build/log-adapters
COPY ./log-adapters .

RUN ./build.sh

WORKDIR /build/lcat
COPY ./lcat .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o lcat .

WORKDIR /build/lq
COPY ./lq .

RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix cgo -o lq .

FROM debian:jessie-slim

RUN apt-get update -y && apt-get install -y ca-certificates

COPY --from=builder /build/lcat/lcat /usr/local/bin
COPY --from=builder /build/lq/lq /usr/local/bin

RUN /bin/mkdir -p /data/lq

COPY --from=builder /build/lq/config.yaml /data/lq

RUN adduser -u 25010 -c 'EPBC Application Owner' epbc

WORKDIR /home/epbc

RUN mkdir adapters

COPY --from=builder /build/log-adapters/*/*.so adapters

USER epbc

ENV prd wss://prd-logs.educationplannerbc.ca
ENV stg wss://stg-logs.educationplannerbc.ca
ENV pvw wss://pvw-logs.educationplannerbc.ca
ENV tst wss://tst-logs.educationplannerbc.ca
ENV dev wss://dev-logs.educationplannerbc.ca

