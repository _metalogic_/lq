package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"time"

	"bitbucket.org/_metalogic_/build"
	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/logs"
	"golang.org/x/crypto/ssh/terminal"
)

type serviceFlags []string

func (i *serviceFlags) String() string {
	return fmt.Sprintf("%v", *i)
}

func (i *serviceFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

var (
	configFlg    string
	dirFlg       string
	levelFlg     logs.LogLevel
	grepFlg      string
	xgrepFlg     string
	tzFlg        string
	serviceFlgs  serviceFlags
	xserviceFlgs serviceFlags
	debugFlg     bool
	helpFlg      bool
	rawFlg       bool
	verboseFlg   bool

	info          build.BuildInfo
	location      *time.Location
	timeLayout    string
	serviceFilter map[string][]string
	filename      string
	conf          *logs.Config
	reader        io.Reader
)

func init() {

	flag.BoolVar(&debugFlg, "debug", false, "enable debug logging for lq")
	flag.BoolVar(&rawFlg, "raw", false, "output raw JSON")
	flag.BoolVar(&verboseFlg, "v", false, "enable verbose output")
	flag.StringVar(&configFlg, "config", config.IfGetenv("CONFIG_PATH", ".:/usr/local/etc/lq"), "search path for lq config")
	flag.StringVar(&dirFlg, "d", config.IfGetenv("ADAPTER_PATH", "./adapters:/usr/local/etc/lq/adapters"), "log adapters directory")
	flag.StringVar(&tzFlg, "tz", "Canada/Pacific", "convert times frm UTC to local timezone")
	flag.StringVar(&grepFlg, "grep", "", "search logs for messages containing grep string")
	flag.StringVar(&xgrepFlg, "xgrep", "", "exclude logs for messages containing grep string")
	flag.Var(&levelFlg, "level", "filter logs at and above log level")
	flag.Var(&serviceFlgs, "source", "filter logs by source or source/component; may be repeated")
	flag.Var(&xserviceFlgs, "xsource", "exclude logs by source or source/component (cannot be combined with source); may be repeated")

	log.SetFlags(0) // do not print timestamp
	log.SetColorize(true)
	timeLayout = "2006-01-02 15:04:05 MST"
	reader = os.Stdin

	info = build.Info

	version := info.String()
	command := info.Name()

	flag.Usage = func() {
		fmt.Printf("Project %s:\n\n", version)
		fmt.Printf("Usage: %s -help (this message) | %s [options]:\n\n", command, command)
		flag.PrintDefaults()
	}
}

func main() {

	flag.Parse()

	if helpFlg {
		flag.Usage()
		return
	}

	var err error

	if verboseFlg {
		logs.SetVerbose(verboseFlg)
	}

	if debugFlg {
		log.SetLevel(log.DebugLevel)
	}
	dir, err := config.DirFromSearchPath(dirFlg)
	if err != nil {
		log.Fatal(err)
	}
	if err = logs.RegisterAdapters(dir.Name()); err != nil {
		log.Fatal(err)
	}

	// f, err := config.FileFromSearchPath(configFlg)
	data, err := config.LoadFromSearchPath("config.yaml", configFlg)
	if err != nil {
		log.Fatalf("when parsing -config: %s", err)
	}

	if conf, err = logs.Configure(data); err != nil {
		log.Fatal(err)
	}

	log.Debugf("Configuration: %+v", conf)

	if tzFlg != "" {
		if location, err = time.LoadLocation(tzFlg); err != nil {
			if verboseFlg {
				// dump /local/go/lib/time/zoneinfo.zip
			}
			log.Fatalf("'%s' is not a valid timezone", tzFlg)
		}
	}

	if grepFlg != "" {
		logs.SetGrep(grepFlg)
	}

	if xgrepFlg != "" {
		logs.SetXGrep(xgrepFlg)
	}

	if len(serviceFlgs) > 0 && len(xserviceFlgs) > 0 {
		log.Fatal("lq usage: flags service and xservice cannot be used together")
	}
	if len(serviceFlgs) > 0 {
		if err = logs.SetServices(serviceFlgs, false); err != nil {
			log.Fatal(err)
		}
	}

	if len(xserviceFlgs) > 0 {
		if err = logs.SetServices(xserviceFlgs, true); err != nil {
			log.Fatal(err)
		}
	}

	if levelFlg == "" {
		levelFlg = logs.Info
	}

	logs.SetLevel(levelFlg)

	if len(flag.Args()) == 1 {
		filename = flag.Args()[0]
		if !terminal.IsTerminal(0) {
			log.Fatalf("file '%s' is being read: cannot also read from STDIN", filename)
		}
		reader, err = os.Open(filename)
		if err != nil {
			log.Fatal(err)
		}
	}

	if debugFlg {
		logs.PrintConfig()
	}
	err = logs.Print(reader, rawFlg)
	if err != nil {
		log.Error(err)
	}
}
